package com.brianslittledarlings.onion.kuppler;

import android.support.v4.app.Fragment;

public class MatchmakingChoiceActivity extends SingleFragmentActivity{
    @Override
    protected Fragment createFragment() {
        return MatchmakingChoiceFragment.newInstance();
    }
}
