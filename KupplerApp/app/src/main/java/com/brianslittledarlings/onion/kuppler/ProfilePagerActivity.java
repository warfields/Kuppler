package com.brianslittledarlings.onion.kuppler;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProfilePagerActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    //Tags for intents
    public static final String TAG = "ProfilePagerActivity";
    private static final String EXTRA_PROFILE_ID =
            "com.brianslittledarlings.onion.kuppler.profile_id";
    private final String EXTRA_WINGMAN = "extra_wingman";
    private final String EXTRA_FIREBASE_WINGMAN_ID = "extra_firebase_wingman_id";
    private final String EXTRA_START_MATCHMAKING = "extra_start_matchmaking";
    private final String BUNDLE = "bundle";

    //local variables
    private boolean mWingmanChoice;
    private boolean mStartMatchmaking;
    private Button mSelectButton;
    private String mCurrentDatabaseId;
    private UUID profileId;
    private String mUnseenId;
    private ViewPager mViewPager;
    private List<Profile> mProfiles;
    private List<String> mUnseenIdList = new ArrayList<>();

    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //prevents a crash where reporting the first profile has no UUID to report
        mCurrentDatabaseId = ProfileLab.get(getBaseContext()).getProfiles().get(0).getmFireBaseUUID();

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabase = mFirebaseDatabase.getReference();

        setContentView(R.layout.activity_list_choice_pager);
        //get firebase user info for menu

        // Configure Google API Client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        //choosing whether we are selecting the "wingman" or "suitors"
        mWingmanChoice = getIntent().getBooleanExtra(EXTRA_WINGMAN,true);
        mStartMatchmaking = getIntent().getBooleanExtra(EXTRA_START_MATCHMAKING, false);

        if (!mWingmanChoice) {
            profileDataPull(getIntent().getStringExtra(EXTRA_FIREBASE_WINGMAN_ID));
            mCurrentDatabaseId = ProfileLab.get(getBaseContext()).getPotentialProfiles().get(0).getmFireBaseUUID();
        }

        //Toolbar setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.pager_toolbar);
        setSupportActionBar(toolbar);

        //grab extra to indicate profile
        profileId = (UUID) getIntent().getSerializableExtra(EXTRA_PROFILE_ID);
        mViewPager = (ViewPager) findViewById(R.id.prof_pager);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //get the firebase UUID of the profile currently displayed in pager
                mCurrentDatabaseId = ProfileLab.get(getBaseContext()).getProfiles().get(position).getmFireBaseUUID();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //get profiles from database into pager via manager
        FragmentManager fragmentManager = getSupportFragmentManager();

        //setup the button
        mSelectButton = (Button) findViewById(R.id.confirm_button);

        //set button text to reflect activity
        if (mWingmanChoice) {
            mSelectButton.setText(R.string.choose_wingman);
        } else {
            mSelectButton.setText(R.string.good_match);
        }

        //TODO: This is really bad practice. the Intent should be created in the onClick listener.
        //button changes based on what activity starts it, and what information it should have
        mSelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent profilePagerIntent = new Intent(getBaseContext(), ProfilePagerActivity.class);
                Intent LoadingInfoIntent = new Intent(getBaseContext(), LoadingInfoActivity.class);
                // Chosen a person
                if (mWingmanChoice) {
                    //restart pager with different pull, based on chosen person
                    ProfilePagerActivity.this.runProfileLab(mCurrentDatabaseId);

                    Bundle bundle = new Bundle();
                    bundle.putString(EXTRA_FIREBASE_WINGMAN_ID, mCurrentDatabaseId);
                    bundle.putBoolean(EXTRA_WINGMAN, false);
                    bundle.putSerializable(EXTRA_PROFILE_ID, profileId);

                    LoadingInfoIntent.putExtra(BUNDLE, bundle);
                    //prevent giant stack of pagers
                    LoadingInfoIntent.setFlags(profilePagerIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);

                    //go to loading screen to give time for query
                    startActivity(LoadingInfoIntent);
                    Toast.makeText(getBaseContext(),"Get to know them more!", Toast.LENGTH_SHORT).show();

                } else {
                    profilePagerIntent.putExtra(EXTRA_WINGMAN,true);

                    //prevent giant stack: no add to history
                    profilePagerIntent.setFlags(profilePagerIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    //alert user of change of activity
                    Toast.makeText(getBaseContext(), "Choose a wingman", Toast.LENGTH_SHORT).show();


                    //Send wingman match to database
                    //get list from firebase, then add to id to list

                    List<String> unseenIdList = stringToList(mUnseenId);

                    //null check, then search for uuid
                    if (mUnseenIdList.size() == 0) {
                        mUnseenIdList.add(mCurrentDatabaseId);
                        //send to database
                    } else if (mUnseenIdList.contains(mCurrentDatabaseId)) {
                        //find index of UUID in list, increase count
                        int index = -1;
                        for (int i = 0; i < mUnseenIdList.size(); i++) {
                            if (mCurrentDatabaseId.equals(mUnseenIdList.get(i))) {
                                index = i;
                                break;
                            }
                        }
                        if (index > -1) {
                            //add both at index
                        }
                    } else {
                        //add both
                        mUnseenIdList.add(mCurrentDatabaseId);
                    }

                    mDatabase = mFirebaseDatabase.getReference();

                    //update wingman matches
                    mDatabase.child("users")
                            .child(getIntent().getStringExtra(EXTRA_FIREBASE_WINGMAN_ID).toString())
                            .child("mUnseenId").setValue(mUnseenIdList);

                    //todo what does this do
                    mDatabase.child("users")
                            .child(getIntent().getStringExtra(EXTRA_FIREBASE_WINGMAN_ID))
                            .child("mName")
                            .push()
                            .setValue("Placeholder"); //TODO make this politically correct

                    //same stuff for match
                    //make variable
                    profileDataPull(mCurrentDatabaseId);

                    unseenIdList = stringToList(mUnseenId);
                    //null case
                    if (unseenIdList == null) {
                        unseenIdList.add(getIntent().getStringExtra(EXTRA_FIREBASE_WINGMAN_ID));
                    }
                    else if (unseenIdList.contains(getIntent().getStringExtra(EXTRA_FIREBASE_WINGMAN_ID))) {
                        //find index of UUID in list, increase count
                        int index = -1;
                        for (int i = 0; i < unseenIdList.size(); i++) {
                            if (getIntent().getStringExtra(EXTRA_FIREBASE_WINGMAN_ID).equals(unseenIdList.get(i))) {
                                index = i;
                                break;
                            }
                        }
                        if (index > -1) {
                            //add both at index
                        } else {
                            //add both
                            unseenIdList.add(getIntent().getStringExtra(EXTRA_FIREBASE_WINGMAN_ID));
                        }
                        mDatabase.child("users")
                                .child(mCurrentDatabaseId)
                                .child("mUnseenId").setValue(mUnseenId);
                    }
                    startActivity(profilePagerIntent);
                }
            }
        });
        //get different profiles is matching for someone or choosing who to match for
        if (mWingmanChoice) {
            mProfiles = ProfileLab.get(this).getProfiles();
        }
        else {
            mProfiles = ProfileLab.get(this).getPotentialProfiles();
        }


        //Make Profiles appear in pager and be swipeable
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            //Get profile list attributes
            @Override
            public Fragment getItem(int position) {
                Profile profile = mProfiles.get(position);
                if (ProfilePagerActivity.this.mWingmanChoice) {
                    return ProfileFragment.newInstance(profile.getmId());
                }
                return ProfileFragment.newInstance(profile.getmFireBaseUUID());
            }

            @Override
            public int getCount() {
                return mProfiles.size();
            }
        });

        //find profile from list and display in pager
        for (int i = 0; i < mProfiles.size(); i++) {
            if (mProfiles.get(i).getmId().equals(profileId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
    //boiler plate options menu stuff
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.report_profile:
                //report, flagged profiles will not be pulled.

                // change the pullable flag to false in firebase, notify user
                Toast.makeText(this, "This profile has been reported and will not show up in future matchmaking", Toast.LENGTH_LONG).show();
                mDatabase.child("users")
                        .child(mCurrentDatabaseId)
                        .child("mVisablePull").setValue(false);

                return true;
            case R.id.edit_profile_menu:
                //start intent for editProfileActivity
                Intent profIntent = new Intent(this, ProfileEditActivity.class);
                //management of stack
                profIntent.setFlags(profIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(profIntent);
                return true;
            case R.id.sign_out_menu:
                //sign user out, go to login page
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);

                //update the current user
                mFirebaseUser = mFirebaseAuth.getCurrentUser();

                //clear the activity stack
                Intent intent = new Intent(this, LoginActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            case R.id.view_matches:
                //go to matches pager
                Intent matches = new Intent(this, MatchesPagerActivity.class);
                startActivity(matches);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.

        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    public void runProfileLab(String firebaseId) {
        ProfileLab.get(getBaseContext()).generatePotentialMatches(firebaseId);
    }

    private List<String> stringToList(String s) {
        if(s !=null && s.length() > 0){
            Log.d(TAG, "stringToList: " + s);

            //remove brackets firebase gives objects
            s = s.replaceAll("\\{", "");
            s = s.replaceAll("\\}", "");

            String[] strings = s.split(", ");
            ArrayList<String> retval = new ArrayList<>();
            //re-zip together list
            for(int i = 0 ; i < strings.length; i++){
                retval.add(strings[i]);
            }
            return retval;
        }
        //else is empty
        return new ArrayList<>();
    }


    public void profileDataPull(final String theUUID) {
        try {
            //walk down tree of Firebase to user
            mDatabase.child("users").child(theUUID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //populate the fields
                    if (dataSnapshot.hasChild("mUnseenId")) {
                        for (DataSnapshot snap : dataSnapshot.child("mUnseenId").getChildren()) {
                            //Get list of matches, ok null because no matches possible
                            mUnseenIdList.add(snap.getValue().toString());
                        }
                    } else {
                        mUnseenId = "";
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    //error occurs, let user know
                    Toast.makeText(getBaseContext(), "Database Error", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
