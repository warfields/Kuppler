package com.brianslittledarlings.onion.kuppler;
import java.util.Map;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.UUID;

public class Profile {
    public static final String TAG = "Profile";
    //todo insert more metrics for matches
    private boolean mVisablePull;
    //data to keep on a profile
    private byte[] mProfpic;
    private UUID mId;
    private String mName, mBio, mFireBaseUUID, mGender, mSeeking;
    private int mZip, mAge;
    private Map mSeenMatches;
    private Map mUnseenMatches;

    //FIrebase database
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mStorage;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;

    //BLOCK OF GETTERS/SETTERS AND CONSTRUCTORS
    public Profile(){this(UUID.randomUUID()); }

    public Profile(UUID id){
        mId = id;
    }

    /**
     * Current constructor used to populate/edit profile in database
     * @param mName
     * @param mBio
     * @param mZip
     * @param mFireBaseUUID Takes in String and handles converting to UUID
     */


    public Profile (String mName, String mBio, int mZip, String mFireBaseUUID,
                    int mAge, String mGender, String mSeeking) {
        this.mName = mName;
        this.mBio = mBio;
        this.mZip = mZip;
        this.mFireBaseUUID = mFireBaseUUID;
        this.mAge = mAge;
        this.mGender = mGender;
        this.mSeeking = mSeeking;
        this.mVisablePull = true;
    }

    public UUID getmId(){ return mId; }

    public byte[] getmProfpic() {
        return mProfpic;
    }

    private void setmProfpic(byte[] mProfpic) {
        this.mProfpic = mProfpic;
    }

    public String getmName() {
        return mName;
    }

    private void setmName(String mName) {
        this.mName = mName;
    }

    public String getmBio() {
        return mBio;
    }

    private void setmBio(String mBio) {
        this.mBio = mBio;
    }

    public int getmZip() {
        return mZip;
    }

    private void setmZip(int mZip) {
        this.mZip = mZip;
    }

    public String getmFireBaseUUID() {
        return mFireBaseUUID;
    }

    public void setmFireBaseUUID(String mFireBaseUUID) {
        this.mFireBaseUUID = mFireBaseUUID;
    }

    public int getmAge() {
        return mAge;
    }

    private void setmAge(int mAge) {
        this.mAge = mAge;
    }

    public String getmGender() {
        return mGender;
    }

    private void setmGender(String mGender) {
        this.mGender = mGender;
    }

    public String getmSeeking() {
        return mSeeking;
    }

    private void setmSeeking(String mSeeking) {
        this.mSeeking = mSeeking;
    }

    public boolean getmVisablePull() {
        return mVisablePull;
    }
    public void setmVisablePull(boolean mVisablePull) {
        this.mVisablePull = mVisablePull;
    }

    private boolean pullProfilePictureFromFirebase() { // if false the pull has failed
        Log.d(TAG, "pullFireBase: Pulling image");
        mFirebaseStorage = FirebaseStorage.getInstance();
        mStorage = mFirebaseStorage.getReference();
        try { //This gets files from firebase with a max of two megabytes
            StorageReference profPic = mStorage.child("ProfilePics/" + mFireBaseUUID + ".jpeg");
            final long HALF_MEGABYTE = 1024 * 750;
            profPic.getBytes(HALF_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    // Data for "images/island.jpg" is returns, use this as needed
                    setmProfpic(bytes);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                }
            });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void pullFireBase() throws Exception {
        if (mFireBaseUUID == null){
            Exception e = new Exception("NO FIREBASE UUID PROVIDED");
            throw e;
        }
        if (!pullProfilePictureFromFirebase()){

            Exception e = new Exception("FAILED TO RETRIEVE FIREBASEDATA");
            throw e;
        }
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabase = mFirebaseDatabase.getReference();

        ValueEventListener getUser = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //risk nulls, shouldnt be null if profile is created, see EditProfileFragment
                Profile.this.setmName(dataSnapshot.child("mName").getValue().toString());
                Profile.this.setmBio(dataSnapshot.child("mBio").getValue().toString());
                Profile.this.setmZip(Integer.parseInt(dataSnapshot.child("mZip").getValue().toString()));
                Profile.this.setmAge(Integer.parseInt(dataSnapshot.child("mAge").getValue().toString()));
                Profile.this.setmGender(dataSnapshot.child("mGender").getValue().toString());
                Profile.this.setmSeeking(dataSnapshot.child("mSeeking").getValue().toString());
                Profile.this.setmVisablePull(Boolean.parseBoolean(dataSnapshot.child("mVisablePull").getValue().toString()));
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.child("users").child(mFireBaseUUID).addListenerForSingleValueEvent(getUser);
    }

    @Override
    public String toString() {
        return mName + " " + mBio + " zip: " + mZip + " gender: " + mGender;
    }
}