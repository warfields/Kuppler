package com.brianslittledarlings.onion.kuppler;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class MatchesPagerActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{
    public static String TAG = "MatchesPagerActivity";
    private ViewPager mViewPager;
    private Button hiddenbutton;
    private List<Profile> mProfiles;
    private UUID profileId;
    private String mUnseenId;
    private String mCurrentprofid;
    private List<String> mUnseen;
    private static final String EXTRA_PROFILE_ID =
            "com.brianslittledarlings.onion.kuppler.profile_id";

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_choice_pager);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabase = mFirebaseDatabase.getReference();

        mProfiles = MatchmakingLab.get(getBaseContext()).getProfiles();
        //get list
        //mUnseen = stringToList(mUnseenId);
        Log.d(TAG, "onCreate: 1");
        profileId = (UUID) getIntent().getSerializableExtra(EXTRA_PROFILE_ID);
        Log.d(TAG, "onCreate: 2");

        mViewPager = (ViewPager) findViewById(R.id.prof_pager);
        Log.d(TAG, "onCreate: 3");

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //get the firebase UUID and assign it to mReportID
                mCurrentprofid = MatchmakingLab.get(getBaseContext()).getProfiles().get(position).getmFireBaseUUID();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        Log.d(TAG, "onCreate: 4");
        hiddenbutton = (Button) findViewById(R.id.confirm_button);
        hiddenbutton.setVisibility(View.GONE);
        FragmentManager fragmentManager = getSupportFragmentManager();

        //Make Profiles appear in pager and be swipeable
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Profile profile = mProfiles.get(position);
                return ProfileFragment.newInstance(profile.getmFireBaseUUID());
            }
            @Override
            public int getCount() {
                return mProfiles.size();
            }
            //find profile from list and display

        });
        Log.d(TAG, "onCreate: 5");

        for (int i = 0; i < mProfiles.size(); i++) {
            if (mProfiles.get(i).getmId().equals(profileId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.

        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }
    private List<String> stringToList(String s) {
        if(s !=null){
            //remove brackets
            s = s.replaceAll("\\{", "");
            s = s.replaceAll("\\}", "");

            String[] strings = s.split(", ");
            List<String> retval = new Vector<>();
            //re-zip together map
            for(int i = 0 ; i < strings.length; i++){
                retval.add(strings[i]);
            }
            return retval;
        }
        //else is empty
        return new Vector<String>();
    }
    public void profileDataStored() {
        mDatabase.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(mFirebaseUser.getUid()) && dataSnapshot.child(mFirebaseUser.getUid()).hasChildren()) {
                    mUnseenId = dataSnapshot.child(mFirebaseUser.getUid()).child("mUnseenIdList").getValue().toString();
                    //make list here

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("You done been bamboozed", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }
//populate the fields

}
