package com.brianslittledarlings.onion.kuppler;

import android.support.v4.app.Fragment;

public class ProfileEditActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return ProfileEditFragment.newInstance();
    }

}
