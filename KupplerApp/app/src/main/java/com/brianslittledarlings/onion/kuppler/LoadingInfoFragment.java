package com.brianslittledarlings.onion.kuppler;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class LoadingInfoFragment extends Fragment {

    private final String EXTRA_FIREBASE_WINGMAN_ID = "extra_firebase_wingman_id";
    private final String EXTRA_START_MATCHMAKING = "extra_start_matchmaking";
    private final String EXTRA_WINGMAN = "extra_wingman";

    private String mFirebaseWingmanId;

    private Profile mProfile;
    private ImageView mProfPic;
    private TextView mName, mAge, mGender, mSeeking, mZip, mBio;
    private Button mStartMatchMakingButton;

    public static LoadingInfoFragment newInstance(@Nullable Bundle bundle){
        LoadingInfoFragment fragment = new LoadingInfoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseWingmanId = this.getArguments().getString(EXTRA_FIREBASE_WINGMAN_ID);
        //get
        mProfile = ProfileLab.get(getContext()).getProfile(mFirebaseWingmanId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_loading_info, container, false);
        mProfile = ProfileLab.get(getContext()).getProfile(mFirebaseWingmanId);

        mProfPic = (ImageView) v.findViewById(R.id.prof_pic);
        mProfPic.setImageBitmap(MakeBitmap(mProfile.getmProfpic()));

        mName = v.findViewById(R.id.name);
        mName.setText(mProfile.getmName());

        //mAge, mGender, mSeeking, mZip
        mAge = v.findViewById(R.id.age);
        mAge.setText("Age: " + mProfile.getmAge());

        mGender = v.findViewById(R.id.gender);
        mGender.setText("Gender: " + mProfile.getmGender());

        mSeeking = v.findViewById(R.id.seeking);
        mSeeking.setText("Seeking: " + mProfile.getmSeeking());

        mZip = v.findViewById(R.id.zip);
        mZip.setText("Zip: " + mProfile.getmZip());

        mBio = v.findViewById(R.id.bio);
        mBio.setText(mProfile.getmBio());

        mStartMatchMakingButton = v.findViewById(R.id.startMatching);
        mStartMatchMakingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent profilePagerIntent = new Intent(getActivity(), ProfilePagerActivity.class);
                profilePagerIntent.putExtra(EXTRA_START_MATCHMAKING, true);
                profilePagerIntent.putExtra(EXTRA_WINGMAN, false);
                profilePagerIntent.putExtra(EXTRA_FIREBASE_WINGMAN_ID, mProfile.getmFireBaseUUID());
                profilePagerIntent.setFlags(profilePagerIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(profilePagerIntent);
            }
        });
        return v;
    }

    //decode image with builtins
    public Bitmap MakeBitmap(byte[] image){
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

}
